create table customer(
customer_id primary key serial not null constraint,
last_name varchar(35) not null,
first_name varchar(35) not null,
)

create table components(
part_id primary key serial not null constraint,
name varchar(100) not null,
)

create table design(
design_id primary key serial not null constraint,
name varchar(100) not null,
)